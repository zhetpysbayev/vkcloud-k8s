FROM ubuntu:latest

# Обновляем список пакетов и устанавливаем необходимые зависимости
RUN apt-get update && apt-get install -y curl bash unzip

# Устанавливаем kubectl и helm
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x kubectl && \
    mv kubectl /usr/local/bin/

RUN curl -LO "https://get.helm.sh/helm-v3.7.1-linux-amd64.tar.gz" && \
    tar -zxvf helm-v3.7.1-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/ && \
    rm -rf linux-amd64 helm-v3.7.1-linux-amd64.tar.gz

# Скачиваем и выполняем client-install.sh
RUN curl -sSL https://hub.mcs.mail.ru/repository/client-keystone-auth/latest/linux/client-install.sh | bash
# Задаем команду по умолчанию
ENTRYPOINT ["/bin/bash"]

